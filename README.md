# README #

* Use the Huffman compression algorithm to encode files
* Only built to work with plain text atm

### How To Use ###

To compress a file

```
java Driver compress sourcePath targetPath
```

To decompress a file

```
java Driver decompress sourcePath targetPath
```

### Notes ###

The Huffman Tree itself is prepended as a header on the binary file. Parent nodes are represented by 0s
and leaf nodes by 1s followed by 8 bits for the character. Because of this, the compression isn't worth
it for smaller files, as the tree overhead can match or outweigh the uncompressed data size. Larger files,
however, can experience a significant reduction in volume.