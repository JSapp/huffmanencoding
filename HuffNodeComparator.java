import java.util.Comparator;

/**
 * Helps sort Huffman Tree Nodes, you like that right?
 * (Hey why write a whole class for this man wha-
 */
public class HuffNodeComparator implements Comparator<Node<Character, Integer>>
{
	@Override
	public int compare(Node<Character, Integer> n1, Node<Character, Integer> n2)
	{
		if(n1.getValue() > n2.getValue())
			return 1;
		else if(n1.getValue() < n2.getValue())
			return -1;
		return 0;
	} // compare
} // HuffNodeComparator