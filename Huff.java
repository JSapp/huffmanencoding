import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Huff
{
	private static void printTree(Node<Character, Integer> node)
	{
		if(node != null){
		System.out.print(node.getKey() + ", ");
		printTree(node.getLeft());
		printTree(node.getRight());}
	} // printTree
	
	/**
	 * Creates a code dictionary from given Huffman Tree
	 *
	 * @param node Root of Huffman Tree
	 * @param dict Nulled string array of length 128
	 * @param code Empty string
	 *
	 * @return Dictionary as a string array. Indices represent characters, values represent code
	 */
	private static String[] buildDict(Node<Character, Integer> node, String[] dict, String code)
	{
		if(node.isLeaf())
		{
			dict[node.getKey()] = code;
			return dict;
		} // if
		else
		{
			return mergeDicts(buildDict(node.getLeft(), dict, code + "0"), buildDict(node.getRight(), dict, code + "1"));
		} // else
	} // buildDict

	
	/**
	 * Builds up one o' them Huffs
	 *
	 * @param table Charater -> frequency int array
	 *
	 * @return Root node of tree
	 */
	private static Node<Character, Integer> buildTree(int[] table)
	{
		Comparator<Node<Character, Integer>> comparator = new HuffNodeComparator();
		PriorityQueue<Node<Character, Integer>> p = new PriorityQueue<Node<Character, Integer>>(128, comparator);
		
		// Build initial priority queue
		for(int i = 0; i < table.length; i++)
			if(table[i] > 0)
				p.add(new Node<Character, Integer>((char)i, table[i]));
		
		// Build that dang tree nahmsayin?
		while(p.size() > 1)
		{
			Node<Character, Integer> left = p.poll();
			Node<Character, Integer> right = p.poll();
			Node<Character, Integer> parent = new Node<Character, Integer>(left.getValue() + right.getValue());
			parent.setLeft(left);
			parent.setRight(right);
			p.add(parent);
		} // while
		
		return p.poll();
	} // buildTree

	/**
	 * Builds a table of character frequencies in a string based on the first 128 ASCII characters
	 *
	 * @param s Input string
	 *
	 * @return Integer array of the first 128 ASCII character frequencies, where char = index and frequency = value
	 */
	private static int[] charFreq(String s)
	{
		int[] table = new int[128];
		
		for(int i = 0; i < s.length(); i++)
			table[s.charAt(i)]++;
		
		return table;
	} // charFreq
	
	/**
	 * Compress given text to given output file
	 *
	 * @param text Text to compress
	 * @param filePath File to write to
	 */
	public static void compress(String text, String filePath)
	{
		// Create character frequency table
		int[] table = charFreq(text);
		
		// Create Huffman Encoding Tree
		Node<Character, Integer> root = buildTree(table);
				
		System.out.print("Starting dictionary...");
		
		// Create code dictionary
		String[] dictionary = buildDict(root, new String[128], "");
		
		System.out.println("Dictionary Completed\n");
		
		System.out.print("Encoding Tree...");
		
		// Encode Huffman Tree as binary
		String treeCode = encodeTree(root);
		
		System.out.println("Tree Encoded\n");
	
		try {
			// Open stream
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filePath));
			
			// Write all the shiznaps
			writeBitString(text, dictionary, out, treeCode);
			
			// Close stream
			out.close();
		} catch(IOException e) {
			System.out.println(e);
		} // catch
	} // compress

	/**
	 * Decompress given file.
	 * Note that this is only guaranteed to work with this specific compression routine
	 *
	 * @param filePath File to read
	 *
	 * @param Decompressed text
	 */
	public static String decompress(String filePath)
	{
		String text = "";
		
		try {
			// Open stream
			DataInputStream in = new DataInputStream(new FileInputStream(filePath));
			
			// String to hold binary code
			String outString = "";

			// Root node of Huffman Tree
			Node<Character, Integer> root;
			
			StringBuilder sb = new StringBuilder(65536); // Big number for the big man
			
			System.out.print("\nReading Code...");
			
			// Read in code
			// This could be a lot more efficient, come back to it when finished
			// Maybe do bit shifting idk
			while(in.available() > 0)
			{
				int inByte = in.readUnsignedByte();
				for(int i = 0; i < Integer.numberOfLeadingZeros(inByte) - 24; i++)
					sb.append("0");
				if(inByte != 0) // 0 has 8 leading 0s, so gotta hardcode this case
					sb.append(Integer.toBinaryString(inByte));
			} // while
			
			outString = sb.toString();
						
			System.out.println("Code Read\n");
			
			// To avoid trailing content, compression uses leading 1s to buffer data
			// Cut out leading 1s
			while(outString.charAt(0) == '1')
				outString = outString.substring(1);
			
			System.out.print("Decoding Tree...");
			
			// Set that there root, that it may yield a bountiful harvest
			// Root value holds offset to skip tree encoding
			root = decodeTree(outString);
			
			System.out.println("Tree Decoded\n");
			
			System.out.print("Decoding Text...");
			
			// Get the goods
			text = decode(root, outString.substring(root.getValue()));
			
			System.out.println("Text Decoded\n");
			
			// Close stream
			in.close();
			
		} catch(IOException e) {
			System.out.println(e);
		} // catch
		
		return text;
	} // decompress

	/**
	 * Decodes a Huffman encoded string
	 *
	 * @param node Root of Huffman Tree
	 * @param code String to decode
	 *
	 * @return Decoded string
	 */
	private static String decode(Node<Character, Integer> root, String code)
	{
		Node<Character, Integer> curr = root;
		String decodedString = "";
		
		StringBuilder sb = new StringBuilder(65536); // ;)
	
		char[] chCode = code.toCharArray();
		
		for(int i = 0; i < chCode.length; i++)
		{
			if(chCode[i] == '0')
				curr = curr.getLeft();
			else
				curr = curr.getRight();
			if(curr.isLeaf())
			{
				sb.append(curr.getKey());
				curr = root;
			} // if
		} // for
		
		decodedString = sb.toString();
		
		return decodedString;
	} // decode
	
	/**
	 * Convert binary string to Huffman Tree
	 *
	 * @param s String to convert
	 *
	 * @return Root of Huffman Tree
	 */
	private static Node<Character, Integer> decodeTree(String s)
	{
		Node<Character, Integer> node = new Node<Character, Integer>(0);
			
		return decodeTreeHelper(s, node);
	} // helper
	
	/**
	 * Helper method for decodeTree
	 *
	 * @param s String to convert
	 * @param node root of Huffman Subtree
	 *
	 * @return Root of Huffman Tree
	 */
	private static Node<Character, Integer> decodeTreeHelper(String s, Node<Character, Integer> node)
	{
		if(s.charAt(0) == '0')
		{
			node.setLeft(decodeTreeHelper(s.substring(1), new Node<Character, Integer>(0)));
			node.setRight(decodeTreeHelper(s.substring(1 + node.getLeft().getValue()), new Node<Character, Integer>(0)));
			node.setValue(node.getLeft().getValue() + node.getRight().getValue() + 1);
			return node;
		} // if
		else
		{
			node.setKey((char)Integer.parseInt(s.substring(1, 9), 2));
			node.setValue(9);
			return node;
		} // else
	} // decodeTree

	/**
	 * Convert Huffman Tree to binary string
	 *
	 * @param node Root of Huffman Tree
	 *
	 * @return Converted tree as binary string
	 */
	private static String encodeTree(Node<Character, Integer> node)
	{
		if(!node.isLeaf())
			return "0" + encodeTree(node.getLeft()) + encodeTree(node.getRight());
		else
		{
			int ch = (int)node.getKey();
			String bits = "";
			
			// Format chars for 8 bits
			for(int i = 0; i < Integer.numberOfLeadingZeros(ch) - 24; i++)
				bits += "0";
			
			bits += Integer.toBinaryString(ch);
			
			return "1" + bits;
		} // else
	} // encodeTree
	
	
	/**
	 * Combine two Huffman dictionaries
	 * Used in buildDict
	 * Ensure inputs are initialized with null values instead of empty strings
	 *
	 * @param array1 Huffman dictionary as a string array of length 128
	 * @param array2 Huffman dictionary as a string array of length 128
	 *
	 * @return Combined dictionary
	 */
	private static String[] mergeDicts(String[] array1, String[] array2)
	{
		String[] newArray = new String[128];
		for(int i = 0; i < 128; i++)
			if(array1[i] != null)
				newArray[i] = array1[i];
			else if(array2[i] != null)
				newArray[i] = array2[i];
			
		return newArray;
	} // mergeDicts
	
	/**
	 * Compress the thing
	 *
	 * @param s Text to compress
	 * @param dict Dictionary of Huffman Tree
	 * @param out Stream to write to
	 */
	private static void writeBitString(String s, String[] dict, DataOutputStream out, String treeCode)
	{
		String binary = treeCode;
		
		System.out.print("Converting String to Binary...");
		
		StringBuilder sb = new StringBuilder(binary.length() + s.length() * 4);
		
		sb.append(binary);
		
		// Convert string to binary
		for(int i = 0; i < s.length(); i++)
		{
			//binary += dict[s.charAt(i)];
			sb.append(dict[s.charAt(i)]);
		}
		
		binary = sb.toString();
		
		System.out.println("String Converted\n");
		
		// Convert binary to actual binary
		int inByte = 0;
		int bit = 7; // counter which runs backwards for byte readability
		int buffer = 8 - (binary.length() % 8); // Readable code always starts with 0, use leading 1s to round out bytes
		
		for(int i = 0; i < buffer; i++)
			binary = "1" + binary;
		
		try {
			System.out.print("Writing File...");
			for(int i = 0; i < binary.length(); i++)
			{
				if(binary.charAt(i) == '0')
					inByte &= ~(1 << bit);
				else
					inByte |= 1 << bit;
				if(--bit < 0)
				{
					out.writeByte(inByte);
					inByte = 0;
					bit = 7;
				} // if
			} // for
			
			System.out.println("File Written\n");
		} catch(IOException e) {
			System.out.println("Ya dun goofed");
		} // catch
	} // writeBitstring
} // Huff