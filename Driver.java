import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.System;
import java.util.Scanner;


public class Driver
{
	public static void main(String[] args)
	{
		if(args.length == 3)
		{
			try {
				// Compress
				if(args[0].equals("compress"))
				{
					// Open scanner
					BufferedReader in = new BufferedReader(new FileReader(new File(args[1])));
		
					// Store file contents
					String input = "";
					String line;
					StringBuilder sb = new StringBuilder(65536);
		
					// Output path
					String outPath = args[2];
		
					System.out.print("\nReading File...");
					
					// Read file
					while((line = in.readLine()) != null)
					{
						 sb.append(line);
						 sb.append("\n");
					} // while
					
					input = sb.toString();
					
					System.out.println("File Read\n");
					
					// Cut last line break
					input = input.substring(0, input.length() - 1);
					
					// Compress the junk
					Huff.compress(input, outPath);
					
					// Close scanner
					in.close();
				} // if
				
				// Decompress
				else if(args[0].equals("decompress"))
				{
					// Input path
					String inPath = args[1];
					
					// Output path
					String outPath = args[2];
					
					// Open writer
					BufferedWriter out = new BufferedWriter(new FileWriter(new File(outPath)));
					
					// Decompress text
					String text = Huff.decompress(inPath);
					
					// Write that noise
					out.write(text);
					
					// Close writer
					out.close();
				} // else if
				
				// Error
				else
					System.out.println("Please see readme for valid commands");
			} catch(IOException e) {
				System.out.println("Unable to locate file " + args[1]);
			} // catch
		} // if
		else
			System.out.println("Please see readme for valid commands");
	} // main
} // Driver