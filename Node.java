/**
 * Huffman Tree Node, works for BSTs in general but there's no parent reference
 *
 */
public class Node<K, V>
{
	private Node<K, V> leftChild = null;
	private Node<K, V> rightChild = null;
	private K key = null;
	private V value = null;
	
	public Node()
	{
	} // Node
	
	public Node(V v)
	{
		value = v;
	} // Node
	
	public Node(K k, V v)
	{
		key = k;
		value = v;
	} // Node
	
	public boolean isLeaf()
	{
		if(leftChild == null && rightChild == null)
			return true;
		else
			return false;
	} // isLeaf
	
	/**
	 * This tree is ancient, go count all the leaves
	 *
	 * @param n Root node of tree
	 *
	 * @return I found this many leaves!
	 */
	public int numLeaves()
	{
		if(isLeaf())
			return 1;
		else
			return getLeft().numLeaves() + getRight().numLeaves();
	} // numLeaves
	
	public K getKey()
	{
		return key;
	} //
	
	public V getValue()
	{
		return value;
	} // getValue
	
	public void setKey(K k)
	{
		key = k;
	} // setKey
	
	public void setValue(V v)
	{
		value = v;
	} // setValue
	
	public Node<K, V> getLeft()
	{
		return leftChild;
	} // getLeft
	
	public Node<K, V> getRight()
	{
		return rightChild;
	}
	
	public void setLeft(Node<K, V> n)
	{
		leftChild = n;
	} // setLeft
	
	public void setRight(Node<K, V> n)
	{
		rightChild = n;
	} // setRight
} // Node